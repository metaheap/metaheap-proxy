var fs = require('fs')
var http = require('http')
var httpProxy = require('http-proxy')

var proxy = httpProxy.createProxyServer({})

const projectPackage = require('./package.json')
console.log(`running: v${projectPackage.version}`)

var server = http.createServer(function (req, res) {
  var host = req.headers.host.split('.')
  // console.log(`host: ${req.headers.host}`)

  if (host.length === 3) {
    if (host[2] === "development") {
      // trabur.metaheap.development
      if (host[0] === "trabur") {
        // console.log(`proxy: ${req.headers.host} -> trabur.metaheap.development`)
        proxy.web(req, res, {
          target: 'http://127.0.0.1:7548' // trabur
        })
      // *.metaheap.development
      } else {
        // console.log(`proxy: ${req.headers.host} -> *.metaheap.development`)
        proxy.web(req, res, {
          target: 'http://127.0.0.1:8374' // frontend
        })
      }
    } else if (host[2] === "prod") {
      // trabur.metaheap.prod
      if (host[0] === "trabur") {
        // console.log(`proxy: ${req.headers.host} -> trabur.metaheap.prod`)
        proxy.web(req, res, {
          target: 'http://127.0.0.1:7548' // trabur
        })
      // *.metaheap.prod
      } else {
        // console.log(`proxy: ${req.headers.host} -> *.metaheap.prod`)
        proxy.web(req, res, {
          target: 'http://127.0.0.1:8374' // frontend
        })
      }
    } else {
      console.log("TLD: " + host[2] + ", has not been implemented.");
    }
  } else if (host.length === 2) {
    // http://metaheap.com -> http://www.metaheap.com
    res.writeHead(301, {
      Location: 'http://www.metaheap.' + host[1]
    });
    res.end();
  }
});

console.log(`server: 127.0.0.1:8080`)
server.listen(8080, '127.0.0.1')
